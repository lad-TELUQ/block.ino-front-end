# block.ino - Front-End

### The project

[block.ino project](http://blockino.ufsc.br/) was orignaly developed at [Remote Experimentation Laboratory (RExLab)](https://rexlab.ufsc.br) in Brazil in 2016.

The back-end application has been upload to a [GitLab repository](https://gitlab.com/lad-TELUQ/block.ino-back-end). This repository describes the front-end app using [Google Blockly library](https://developers.google.com/blockly/) and [Materialize CSS](https://materializecss.com/).

### Development

##### Building new blocks
block.ino might need some specif blocks for untested sensors. In this sense we strongly recomend you to build blocks using [blockly developer tools](https://blockly-demo.appspot.com/static/demos/blockfactory/index.html).

Blocks must be included on ```blockly``` directory. Code generators must be included on ```blockly/generators/arduino```. Blocks layout design must be included on ```blockly/blocks/arduino```.

##### Custom blocks
One of the examples of custom blockly design/layout file is the ```DHT11.js```. This file was created to support DHT11 (Temperature & Humidity) sensor.

```DHT11.js``` design/layout file:
```
'use strict';

goog.provide('Blockly.Blocks.Arduino.DHT11');

goog.require('Blockly.Arduino');
goog.require('Blockly.StaticTyping');

Blockly.Blocks.Arduino.DHT11.HUE = 290;

Blockly.Blocks['dht11_value'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.Dht11_Description)//"Sensor DHT11 - valor da")
        .appendField(new Blockly.FieldDropdown([[Blockly.Msg.Dht11_Temperature, Blockly.Msg.temperature], [Blockly.Msg.Dht11_Humidity, Blockly.Msg.humidity]]), "option");
    this.setOutput(true, null);
    this.setColour(Blockly.Blocks.Arduino.DHT11.HUE);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['dht11_read'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.Dht11)//"Ler sensor DHT11 no pino")
        .appendField(new Blockly.FieldDropdown([["A1", "A1"], ["A2", "A2"], ["A3", "A3"], ["A4", "A4"], ["A5", "A5"]]), "pin");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(Blockly.Blocks.Arduino.DHT11.HUE);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
````

The previous file must match with the generator file, in which will be named as the same name from the previous. ```DHT11.js``` file example:

```
'use strict';

goog.provide('Blockly.Arduino.DHT11');

goog.require('Blockly.Arduino');


Blockly.Arduino['dht11_value'] = function (block) {
    Blockly.Arduino.addInclude('dht11', '#include <dht.h>');
    Blockly.Arduino.addDeclaration('dht11', 'dht DHT;');
    
    var dropdown_option = block.getFieldValue('option');
    // TODO: Assemble JavaScript into code variable.
    console.log(Blockly.Msg.humidity == dropdown_option);
    
    console.log(dropdown_option);
    var code = ''; 
    if(Blockly.Msg.humidity == dropdown_option){ 
        code = 'DHT.humidity';
    }else{
        code = 'DHT.temperature';
    }
    // TODO: Change ORDER_NONE to the correct strength.
    return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino['dht11_read'] = function (block) {
    Blockly.Arduino.addInclude('dht11', '#include <dht.h>');
    Blockly.Arduino.addDeclaration('dht11', 'dht DHT;');
    var dropdown_pin = block.getFieldValue('pin');
    return 'DHT.read11(' + dropdown_pin + ');\n';
};

```


### Production

block.ino uses [socket.io](http://socket.io) library to exchange messages. The communication URL must be edited on blockinoserver_ws.js at [line 11](https://gitlab.com/lad-TELUQ/block.ino-front-end/blob/master/blockinoserver_ws.js#L11).

The following URL must be the LabServer IP address. This URL will enable users to connect and exchange messages with the back-end application.

Example:
```
var addr = 'http://207.162.28.38';
```

block.ino Front-End can be hosted on Apache, nginx or any other web-server. The following example describe the ```blockino.conf``` file for Apache2.

### License

[MIT](https://opensource.org/licenses/MIT)
